## MANNINE mannine/x-push-notification

Отпрака Push сообщений на сервера Apple(apns) && Google (gcm)

### Установка

 - package.json
```json

 "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/mannineteam/x-push-notification"
        }
    ],
    "require": {
		...
        "mannine/x-push-notification"   : "v0.3",
        ...
	},
```
- app/config/app.php

```java

	'providers' => array(
		...
		'Mannine\XPushNotification\XPushNotificationServiceProvider',
		...
	),
	'aliases' => array(
		...
		'Push' => 'Mannine\XPushNotification\Facades\PushNotification'
		...
	)

```
- *composer update*
- *php artisan dump-autoload*

### Использование
```java

	Push::adapter('Apple/Google/Web')
        ->to(array('device_push_id_1','device_push_id_2'))
        ->message(array('content-available' => 1, 'sound'=>''))
    ->send();

```