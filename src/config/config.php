<?php

return array(
    'apple' => array(
        'environment'   => 'dev',
        'certificates'  => array(
            'dev'         => app_path().'/config/certificates/apns-dev-cert.pem',
            'sandbox'     => app_path().'/config/certificates/apns-dev-cert.pem',
            'production'  => app_path().'/config/certificates/apns-prod-cert.pem',
        ),
        'passPhrase'    => 'opus666',
        'service'       => 'apns'
    ),
    'android'=>array(
        'environment'   => 'production',
        'apiKey'        => 'yourAPIKey',
        'service'       => 'gcm'
    )
);
