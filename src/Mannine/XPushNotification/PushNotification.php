<?php namespace Mannine\XPushNotification;

/**
 * Class PushNotification
 * @package Mannine\XPushNotification
 */
class PushNotification {

    /**
     * Current adapter
     * @var
     */
    protected $adapter;

    /**
     * @var
     */
    protected $config;

    /**
     * Message
     * @var
     */
    protected $message = array();

    /**
     * List of adresses
     * @var
     */
    protected $adresses = array();


    public function __construct($config){
        $this->config = $config;
    }

    /**
     * Set type adapter
     * @param $adapter
     * @return $this
     */
    public function adapter($adapter){
        $this->adapter = $adapter;
        return $this;
    }

    /**
     * Set message
     * @param array $message
     * @return $this
     */
    public function message(Array $message){
        $this->message = $message;
        return $this;
    }

    /**
     * Set addresses
     * @param array $adresses
     * @return $this
     */
    public function to(Array $adresses){
        $this->adresses = $adresses;
        return $this;
    }

    /**
     * Send
     * @throws \Exception
     */
    public function send(){

        if(!isset($this->config))
            throw new \Exception('Environment is not setted');
        
        if(is_null($this->adapter))
            throw new \Exception('Adapter was not setted');

        if(count($this->adresses) == 0)
            throw new \Exception('List of addresses was not setted');

        $typeName = "Mannine\\XPushNotification\\Types\\{$this->adapter}\\Push";
        if(!class_exists($typeName))
            throw new \Exception('Adapter was not found');

        $adapter = new $typeName($this->config, $this->adresses, $this->message);

        return $adapter->send();
    }

}