<?php namespace Mannine\XPushNotification\Types\Google;


class Push {

    /**
     * List of Push hosts
     * @var array
     */
    private $host   = "https://android.googleapis.com/gcm/send";

    /**
     * Config
     * @var array
     */
    private $config = array();

    /**
     * Google API key
     * @var
     */
    private $apiKey;

    /**
     * @var array
     */
    protected $addresses = array();

    /**
     * @var array
     */
    protected $payload = array();

    /**
     * @param $config
     * @param $addresses
     * @param $payload
     */
    public function __construct($config, $addresses, $payload){
        $this->config           = $config["google"];
        $this->addresses        = $addresses;
        $this->payload["aps"]   = $payload;

        $this->apiKey = $this->config["apiKey"];
    }

    /**
     * Send message to apple services
     * @return array
     */
    public function send(){

        // Set POST variables (device IDs and payload)
        $fields = array(
            'registration_ids'  => $this->addresses,
            'data'              => $this->payload,
        );

        // Set request headers (authentication and payload type)
        $headers = array(
            'Authorization: key=' . $this->apiKey,
            'Content-Type: application/json'
        );

        // Open connection
        $ch = curl_init();

        // Set the url
        curl_setopt( $ch, CURLOPT_URL, $this->host );

        // Set request method to POST
        curl_setopt( $ch, CURLOPT_POST, true );

        // Set custom headers
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);

        // Get response back as string
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Set post data
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

        // Send the request
        $result = curl_exec($ch);

        // Close connection
        curl_close($ch);
    }

}