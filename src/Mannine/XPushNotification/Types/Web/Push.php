<?php namespace Mannine\XPushNotification\Types\Web;


class Push {

    /**
     * List of Push hosts
     * @var array
     */
    private $host   = "";

    /**
     * Config
     * @var array
     */
    private $config = array();

    /**
     * Certificate file
     * @var
     */
    private $certificate;

    /**
     * @var
     */
    private $passphrase;

    /**
     * @var array
     */
    protected $addresses = array();

    /**
     * @var array
     */
    protected $payload = array();

    /**
     * @param $config
     * @param $addresses
     * @param $payload
     */
    public function __construct($config, $addresses, $payload){
        $this->config           = $config["apple"];
        $this->addresses        = $addresses;
        $this->payload["aps"]   = $payload;

        $this->passphrase = $this->config["passPhrase"];
    }

    /**
     * Send message to apple services
     * @return array
     */
    public function send(){

    }

}