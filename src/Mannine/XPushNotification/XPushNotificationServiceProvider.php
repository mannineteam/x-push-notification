<?php namespace Mannine\XPushNotification;

use Illuminate\Support\ServiceProvider;
use Mannine\XPushNotification\PushNotification;

class XPushNotificationServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot(){
        $this->package('mannine/x-push-notification');
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register(){
        $this->app['push'] = $this->app->share(function($app){
            return new PushNotification($app["config"]->get('x-push-notification::config'));
        });
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides(){
		return array();
	}
}
